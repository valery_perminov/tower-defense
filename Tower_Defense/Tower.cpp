/*
Commands to compile and run:

    g++ -o Tower Tower.cpp -L/usr/X11R6/lib -lX11 -lstdc++
    ./Tower

Note: the -L option and -lstdc++ may not be needed on some machines.

 */


#include <cstdlib>
#include <iostream>
#include <list>
#include <unistd.h>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>


/*
 * X stuff
 */
#include <X11/Xlib.h>
#include <X11/Xutil.h>

using namespace std;

const int BufferSize = 10;
const int FPS = 3;
int latestwave = -1;
int towerX = -1;
int towerY = -1;
int towerWidth = 60;
int towerHeight = 60;

int towerHealth = 10;
int money = 100;


void activateTowers();

/*
 * Information to draw on the window.
 */
struct XInfo {
    Display* display;
    int screen;
    Window window;
    int width;
    int height;
    Pixmap pixmap;
};


//Custom GC class
class CustomGC{
public:
    CustomGC(XInfo &xInfo, char foregroundColor[], char backgroundColor[], int fill_style, int thickness ,int line_style, int cap_style, int join_style){
        /* foreground color settings */
        cm = DefaultColormap(xInfo.display, 0);
        XParseColor(xInfo.display, cm, foregroundColor, &color);
        XAllocColor(xInfo.display, cm, &color);
        
        /* background color settings */
        cm2 = DefaultColormap(xInfo.display, 0);
        XParseColor(xInfo.display, cm2, backgroundColor, &color2);
        XAllocColor(xInfo.display, cm2, &color2);
        
        gc = XCreateGC(xInfo.display, xInfo.window, 0, 0);
        XSetForeground(xInfo.display, gc, color.pixel);
        XSetBackground(xInfo.display, gc, color2.pixel);
        XSetFillStyle(xInfo.display, gc, fill_style);
        XSetLineAttributes(xInfo.display, gc,
                thickness, line_style, cap_style, join_style);
    }
    GC getGC(){
        return gc;
    }
private:
    XColor color;
    XColor color2;
    Colormap cm;
    Colormap cm2;
    XFontStruct* fontInfo;
    char* fontName;
    GC gc;
};

/*
 * An abstract class representing displayable things.
 * Reference for code: course notes 
 */
class Displayable {
public:
    virtual void paint(XInfo &xinfo, CustomGC gc[]) = 0;
    virtual int get_status() = 0; 
};

class Wave {
public:

    Wave() {
        status = 0; //inactive
    }

    int get_status() {
        return status;
    }
    
    int set_status(int s){
        status = s;
        
    }
private:
    int status;
    
};

Wave wave_list[10];

/* check if any wave is currently active*/
int isGameInMotion(){
    for(int i = 0 ; i < 10; i++ ){
        if(wave_list[i].get_status()==1){
            return 1;
        }
    }
    return 0;
}

void drawText(XInfo &xinfo, GC gc, int x_coordinate, int y_coordinate, std::string d_text, char* font_name) {

    //load a larger font
    XFontStruct* font;
    font = XLoadQueryFont(xinfo.display, font_name);
    XSetFont(xinfo.display, gc, font->fid);

    //draw text
    std::string text(d_text);
    XDrawImageString(xinfo.display, xinfo.window, gc, x_coordinate, y_coordinate, text.c_str(), text.length());
}


/*
 * Reference for code: course notes
 */
class Monster : public Displayable {
public:

    virtual void paint(XInfo &xinfo, CustomGC gc[]) {
        XFillArc(xinfo.display, xinfo.pixmap, gc[0].getGC(), x, y, diameter, diameter, 0, 360 * 64);
    }

    void move(XInfo &xinfo, CustomGC gc[]) {
        if(status!=1){
            if (x < 142 && y >= 275 && y <= 281) {
                x = x + speed;
            } else if (x >= 142 && x <= 148 && y >= 109 ) {
                y = y - speed;
            } else if (x < 342 && y >= 105 && y <= 113) {
                x = x + speed;
            } else if (x >= 342 && x <= 348 && y <= 451) {
                y = y + speed;
            } else if (x < 542 && y >= 451 && y <= 457) {
                x = x + speed;
            } else if (x >= 542 && x <= 548 && y >= 279) {
                y = y - speed;
            } else if (x < 700 && y >= 275 && y <= 285) {
                x = x + speed;
                if (x >= 690){
                    towerHealth--;
                    wave_list[wave].set_status(0);
                    for(int i = 0; i < 10-towerHealth; i++){
                        XFillRectangle(xinfo.display, xinfo.window, gc[2].getGC(), 460+((9-i)*20), 11, 10, 29);
                    }
                    this->status = 1;
                }
            }
        }
    }

    int get_status(){
        return status;
    }
    
    void set_status(int s){
        status = s;
    }
    
    int getWave(){
        return wave;
    }
    
    void setWave(int w){
        wave = w;
    }
    
    int getX() {
        return x;
    }

    int getY() {
        return y;
    }
    
    int getHealth(){
        return health;
    }
    
    void setHealth(int hp){
        health = hp;
    }
    
    int getSpeed(){
        return speed;
    }
    
    void setSpeed(int s){
        speed = s;
    }

    Monster(){
        speed = 4;
        health = 100;
        status = 0;
    }
    
    Monster(int x, int y, int diameter) : x(x), y(y), diameter(diameter) {
        speed = 4;
        health = 100;
        status = 0;
    }

private:
    int x;
    int y;
    int diameter;
    int speed;
    int wave;
    int status; //status = 1, if monster reaches tower or gets shot down.
    int health;
};

list<Displayable *> dList; // list of Displayables
vector <Monster> monster;

int getPriorityWave(){
    for(int i = 0 ; i < 10; i++){
        if(wave_list[i].get_status()==1){
            return i;
        }
    }
    return latestwave;
}

void repaintBalance(XInfo &xinfo, CustomGC gc[]){
    std::stringstream ss;
    ss << money;
    string smoney = ss.str();
    drawText(xinfo, gc[10].getGC(), 90, 35, smoney, "12x24");
}

/*
 * Reference for code: course notes
 */
class Bullet : public Displayable {
public:

    virtual void paint(XInfo &xinfo, CustomGC gc[]) {
        XFillArc(xinfo.display, xinfo.pixmap, gc[0].getGC(), x, y, diameter, diameter, 0, 360 * 64);
    }

    void move(XInfo &xinfo, CustomGC gc[]) {
        int enemyX;
        int enemyY;
        int prioritywave;
        prioritywave = getPriorityWave();
        enemyX = monster[prioritywave].getX();
        enemyY = monster[prioritywave].getY();
        if(monster[prioritywave].get_status()==1){
            this->set_status(1);
        }
        if(x!=enemyX && x!=enemyX-1 && x!=enemyX+1 && x!=enemyX+2 && x!=enemyX-2){
            x = x + (((enemyX-x)/(abs(enemyX-x)))*(speed/2));
        }
        if(y!=enemyY && y!=enemyY-1 && y!=enemyY+1 && y!=enemyY+2 && y!=enemyY-2){
            y = y + (((enemyY-y)/(abs(enemyY-y)))*(speed/2));
        }
        if((x<=enemyX+5 && x>=enemyX-5) && (y<=enemyY+5 && y>=enemyY-5)){
            if(monster[prioritywave].getHealth()>0){
                monster[prioritywave].setHealth(monster[prioritywave].getHealth()-10);
            }
            this->set_status(1);
            if(monster[prioritywave].getHealth() == 0){
                monster[prioritywave].set_status(1);
                wave_list[prioritywave].set_status(0);
                money+=100*(prioritywave+1);
                //redraw money
                repaintBalance(xinfo, gc);
                
            }   
        }   
    }

    int get_status(){
        return status;
    }
    
    void set_status(int s){
        status = s;
    }
    
    int getX() {
        return x;
    }

    int getY() {
        return y;
    }
    
    void setX(int setx){
        x = setx;
    }
    
    void setY(int sety){
        y = sety;
    }
    
    int getID(){
        return id;
    }
    
    void setID(int i){
        id = i;
    }
    
    int getSpeed(){
        return speed;
    }
    
    void setSpeed(int s){
        speed = s;
    }
    
    int getType(){
        return type;
    }
    
    void setType(int t){
       type = t; 
    }

    Bullet(){
        speed = 12;
        status = 0;
    }
    
    Bullet(int x, int y, int diameter) : x(x), y(y), diameter(diameter) {
        speed = 12;
        status = 0;
    }

private:
    int x;
    int y;
    int diameter;
    int speed;
    int wave;
    int status; //1 if bullet reaches target
    int type;
    int id;
};

list <Bullet*> bullets;
class Tower{
public:
    virtual void paint(XInfo &xinfo, CustomGC gc[]) {
       XDrawRectangle(xinfo.display, xinfo.pixmap, gc[0].getGC(), x, y, width, height);
    }
    int getStatus(){
        return status;
    }
    
    int setStatus(int s){
        status = s;
    }
    
    int getInstalled(){
        return installed;
    }
    
    int setInstalled(int i){
        installed = i;
    }
    
    int getX() {
        return x;
    }

    int getY() {
        return y;
    }
    
    void setX(int setx){
        x = setx;
    }
    
    void setY(int sety){
        y = sety;
    }
    
    int getType(){
        return type;
    }
    
    void setType(int id){
        type = id;
    }
    
    int getID(){
        return id;
    }
    
    void setID(int i){
        id = i;
    }
     
     
    
    Tower(){
        speed = 4;
    }
    
    Tower(int x, int y, int width, int height) : x(x), y(y), width(width), height(height) {
        speed = 4;
    }
    
private:
    int installed;
    int status;
    int x;
    int y;
    int width;
    int height;
    int speed;
    int type;
    int id;
    //vector <Bullet> bullets;
};

vector <Tower> towers;

int isGameDone(){
    for(int i = 0; i < 10; i++ ){
        if(monster[i].get_status()==0){
            return 0;
        }
    }
    return 1;
}

void drawRectangle(XInfo &xinfo, GC gc, int x_coordinate, int y_coordinate, int width, int height) {
    Display *display = xinfo.display;
    Window win = xinfo.window;

    XWindowAttributes windowinfo;
    XGetWindowAttributes(display, win, &windowinfo);
    // max m and y coordinate
    unsigned int maxHeight = windowinfo.height - 1;
    unsigned int maxWidth = windowinfo.width - 1;

    /* draw side panel on the right side */
    int w = 100; //panel width
    int h = 600; //panel height
    XDrawRectangle(display, win, gc, x_coordinate, y_coordinate, width, height);
}

void drawGrid(XInfo &xInfo, CustomGC gc[]) {
    for (int i = 0; i < 28 * 20; i = i + 20) {
        XDrawLine(xInfo.display, xInfo.pixmap, gc[5].getGC(), 0, 1 + i, 690, 1 + i);
    }
    for (int i = 0; i < 34 * 20; i = i + 20) {
        XDrawLine(xInfo.display, xInfo.pixmap, gc[5].getGC(), 0 + i, 1, 0 + i, 561);
    }
    
}

class Button {
public:
    Button();

    Button(string bname, int x, int y, int w, int h) {
        name = bname;
        x1 = x;
        y1 = y;
        width = w;
        height = h;
        activated = 0;
    }
    int getY(){
        return y1;
    }
    
    int getX(){
        return x1;
    }
    
    int get_Width(){
        return width;
    }
    
    int get_Height(){
        return height;
    }
    
    int get_Status() { // If button has been activated , return 1;
        return activated;
    }
    
    void set_Status(int status) {
        activated = status;
    }
    

    void drawButtonInfoUI(XInfo &xInfo, CustomGC gc[]) {
        if (this->get_Status() == 1) {
            drawRectangle(xInfo, gc[0].getGC(), 710, 360, 80, 330);
            XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 711, 361, 79, 329);
            if(this->name == "Light Cannon"){
                drawText(xInfo, gc[4].getGC(), 730, 375, "LIGHT","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 390, "CANNON","9x15bold");
                XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), 710, 395, 790, 395);
                drawText(xInfo, gc[4].getGC(), 725, 420, "Speed:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 740, 435, "12","9x15bold");
                drawText(xInfo, gc[4].getGC(), 730, 460, "Cost:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 735, 475, "100","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 500, "Class:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 744, 515, "L","9x15bold");
            }else if(this->name == "Heavy Cannon"){
                drawText(xInfo, gc[4].getGC(), 730, 375, "HEAVY","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 390, "CANNON","9x15bold");
                XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), 710, 395, 790, 395);
                drawText(xInfo, gc[4].getGC(), 725, 420, "Speed:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 740, 435, "24","9x15bold");
                drawText(xInfo, gc[4].getGC(), 730, 460, "Cost:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 735, 475, "200","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 500, "Class:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 744, 515, "H","9x15bold");
            }else if(this->name == "Super Cannon"){
                drawText(xInfo, gc[4].getGC(), 730, 375, "SUPER","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 390, "CANNON","9x15bold");
                XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), 710, 395, 790, 395);
                drawText(xInfo, gc[4].getGC(), 725, 420, "Speed:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 740, 435, "30","9x15bold");
                drawText(xInfo, gc[4].getGC(), 730, 460, "Cost:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 735, 475, "500","9x15bold");
                drawText(xInfo, gc[4].getGC(), 725, 500, "Class:","9x15bold");
                drawText(xInfo, gc[4].getGC(), 744, 515, "S","9x15bold");
            }
        } else {
            drawRectangle(xInfo, gc[2].getGC(), 710, 360, 80, 330);
            XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 710, 360, 81, 331);
        }
    }

    void drawButton(XInfo &xInfo, CustomGC gc[]) {
        XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), x1, y1, x1 + width, y1);
        XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), x1, y1, x1, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), x1, y1 + height, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), x1 + width, y1, x1 + width, y1 + height);
    }

    void clickAnimation(XInfo &xInfo, CustomGC gc[]) {
        XDrawLine(xInfo.display, xInfo.window, gc[3].getGC(), x1, y1 + height, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[3].getGC(), x1 + width, y1, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), x1, y1 + height, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), x1 + width, y1, x1 + width, y1 + height);
    }

    void releaseAnimation(XInfo &xInfo, CustomGC gc[]) {
        XDrawLine(xInfo.display, xInfo.window, gc[2].getGC(), x1, y1 + height, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[2].getGC(), x1 + width, y1, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), x1, y1 + height, x1 + width, y1 + height);
        XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), x1 + width, y1, x1 + width, y1 + height);
        if (activated == 0) {
            activated = 1;
        } else {
            activated = 0;
        }
    }
private:
    /* x1 and y1 is top left corner of the button */
    string name;
    int x1;
    int y1;
    int width;
    int height;
    int activated;

};

int splashScreen;
Button pauseButton("Pause", 20, 650, 30, 30);
Button playButton("Play", 53, 650, 30, 30);
Button fastButton("Fast-Forward", 86, 650, 30, 30);
Button waveButton("Next wave", 320, 650, 120, 30);
Button playGameButton("Play Game", 320, 650, 120, 30);
Button lightCannonButton("Light Cannon", 725, 75, 50, 50);
Button heavyCannonButton("Heavy Cannon", 725, 140, 50, 50);
Button superCannonButton("Super Cannon", 725, 205, 50, 50);

/*
 * Function to put out a message on error exits.
 */
void error(string str) {
    cerr << str << endl;
    exit(0);
}

/*
 * update width and height when window
 */

/*
 * Create a window
 */
void initX(int argc, char* argv[], XInfo& xInfo) {

    XSizeHints hints;
    hints.x = 10;
    hints.y = 10;
    hints.width = 690;
    hints.height = 560;
    hints.flags = PPosition | PSize;
    /*
     * Display opening uses the DISPLAY	environment variable.
     * It can go wrong if DISPLAY isn't set, or you don't have permission.
     */
    xInfo.display = XOpenDisplay("");
    if (!xInfo.display) {
        error("Can't open display.");
    }

    for(int i = 0; i < 10; i++ ){
        monster.push_back(Monster(50, 275, 20));
        monster[i].setHealth((i+1)*100);
    }
    /*
     * Find out some things about the display you're using.
     */
    xInfo.screen = DefaultScreen(xInfo.display); // macro to get default screen index

    unsigned long white, black;
    white = XWhitePixel(xInfo.display, xInfo.screen);
    black = XBlackPixel(xInfo.display, xInfo.screen);

    xInfo.window = XCreateSimpleWindow(
            xInfo.display, // display where window appears
            DefaultRootWindow(xInfo.display), // window's parent in window tree
            10, 10, // upper left corner location
            800, 700, // size of the window
            5, // width of window's border
            black, // window border colour
            white); // window background colour

    // extra window properties like a window title
    XSetStandardProperties(
            xInfo.display, // display containing the window
            xInfo.window, // window whose properties are set
            "Tower Defense", // window's title
            "TD", // icon's title
            None, // pixmap for the icon
            argv, argc, // applications command line args
            None); // size hints for the window

    int depth = DefaultDepth(xInfo.display, DefaultScreen(xInfo.display));
    xInfo.pixmap = XCreatePixmap(xInfo.display, xInfo.window, hints.width, hints.height, depth);

    /*
     * Don't paint the background -- reduce flickering
     */
    XSetWindowBackgroundPixmap(xInfo.display, xInfo.window, None);

    /*
     * Put the window on the screen.
     */
    XMapRaised(xInfo.display, xInfo.window);

    XFlush(xInfo.display);
    sleep(2); // let server get set up before sending drawing commands
}


void drawSplash(XInfo &xInfo, CustomGC gc[]) {
    /*
    /* Make splash screen */
    /* Draw background */
    drawRectangle(xInfo, gc[8].getGC(), 0, 0, 800, 700);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 0, 0, 800, 700);
    
    splashScreen = 1;
    
    /* Title decor */
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 310, 70, 175, 5);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 310, 105, 175, 5);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 315, 75, 165, 30);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 315, 75, 165, 30);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 315, 75, 165, 30);
    drawText(xInfo, gc[4].getGC(), 324, 100, "TOWER DEFENSE","12x24");
    
    /* Play game button*/
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 320, 650, 120, 30);
    playGameButton.drawButton(xInfo, gc);
    drawText(xInfo, gc[0].getGC(), 327, 677, "PLAY GAME","12x24");
    
    
    /* Textbox */
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 50, 150, 700, 470);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 50,150,10,470);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 50, 150, 700, 10);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 750, 150, 10, 470);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 50, 620, 710, 10);
    
    drawText(xInfo, gc[0].getGC(), 60, 190, "Instructions:","9x15bold");
    drawText(xInfo, gc[0].getGC(), 60, 205, " - At the top left corner is your current balance (Money). To gain more money, ","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 220, "you must eliminate monsters.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 235, " - Near the top right corner is your health bar. If you let monsters get through,","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 250, " your health will go down.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 265, " - Near the bottom right corner is \"wave bar\". Wave bar is used to track your","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 280, "progression in the game. Press \"NEXT WAVE\" Button (or space bar) to release","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 295, "the next monster.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 310, "- At the bottom left corner there are 3 button (In order: Pause, Play, Fastforward)","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 325, "        - You can press pause button (or key 'p') to pause the game.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 340, "        - You can press the play button (or key 'r') to resume the game.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 355, "        - You can press the fast-forward button (or key 'f'), to fast-forward.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 370, " - At the very Top right corner of your screen you will notice a \"BUY\" label. Below ","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 385, " the \"BUY\" label you can purchase your towers.","heb8x13");   
    drawText(xInfo, gc[0].getGC(), 60, 400, " - You should see 3 buttons below the \"BUY\" label. (Labeled \"L\", \"H\", and \"S\") ","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 415, "Pressing on one of the buttons will select the tower (your weapon).","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 430, "After selecting the tower, move your mouse over to the map (blue area)","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 445, "and click on any blue empty area. After clicking on any empty blue area ","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 460, "you will notice a green rectangle outline. The green line is just a preview.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 475, "To finalize your tower placement, you need to click again. (In other words","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 490, "you need to click twice in the spot to place a tower.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 505, "Furthermore, when you select a tower, you should notice an information window","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 520, "in the bottom right corner. ","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 535, "The information window provides details about the selected tower.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 550, "","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 565, "Press Enter on you keyboard or the Play Game button to start the game.","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 580, "","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 595, "Creator: Valery Perminov","heb8x13");
    drawText(xInfo, gc[0].getGC(), 60, 610, "User ID: 20426569","heb8x13");    
   
}

void drawUI(XInfo &xInfo, CustomGC gc[]) {
    
    /* Draw Sky */
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 0, 60, 690, 100);
    
    /* Draw waves and water*/
    XPoint points[4];
    for(int i = 0; i < 23; i++){
        points[0].x = i*30;    points[0].y = 110;
        points[1].x = (i*30)+30;    points[1].y = 110;
        points[2].x = (i*30)+50;    points[2].y = 80;
        points[3].x = (i*30)+25;    points[3].y = 90;
        XFillPolygon(xInfo.display, xInfo.window, gc[8].getGC(), points, 4, Complex, CoordModeOrigin);
    }
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 0, 110, 690, 15);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 0, 125, 100, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 0, 400, 300, 220);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 200, 225, 100, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 300, 575, 390, 45);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 400, 125, 290, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 400, 300, 100, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[8].getGC(), 600, 400, 90, 175);
    
    //Color stuff
    /* Top panel*/
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 0, 0, 690, 60);
    /* Bottom panel */
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 0, 620, 690, 100);
    
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 700, -1, 100, 350);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 700, 350, 100, 350);
    XFillRectangle(xInfo.display, xInfo.window, gc[11].getGC(), 690, -1, 5, 700);
    XFillRectangle(xInfo.display, xInfo.window, gc[9].getGC(), 700,0, 100, 700/12);
    
    // draw side panel /
    drawRectangle(xInfo, gc[0].getGC(), 700, -1, 100, 700 / 2);
    drawRectangle(xInfo, gc[0].getGC(), 700, 350, 100, 700 / 2);
    drawRectangle(xInfo, gc[0].getGC(), 690, -1, 5, 700);
    drawRectangle(xInfo, gc[0].getGC(), 700, 0, 100, 700 / 12);

    // draw health bar
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 450, 10, 210, 30);
    drawRectangle(xInfo, gc[0].getGC(), 450, 10, 210, 30);
    for(int i = 0; i < towerHealth; i++ ){
       XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 460+(i*20), 10, 10, 30);
    }

    
    // draw all texts
    drawText(xInfo, gc[10].getGC(), 732, 35, "BUY", "12x24");
    drawText(xInfo, gc[10].getGC(), 20 + 345, 35, "HEALTH:", "12x24");
    drawText(xInfo, gc[10].getGC(), 20, 35, "MONEY:", "12x24");
    repaintBalance(xInfo, gc);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 320, 650, 120, 30); //waveButton 
    drawText(xInfo, gc[0].getGC(), 327, 677, "NEXT WAVE", "12x24");

    // draw the map
    /* Tunnel */
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 0, 300, 200, 100);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 100, 125, 100, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 200, 125, 200, 100);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 300, 225, 100, 350);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 300, 475, 300, 100);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 500, 300, 100, 175);
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 500, 300, 190, 100);
    
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 0, 300, 100, 300);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 100, 300, 100, 125);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 0, 700 - 300, 200, 700 - 300);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 100, 125, 400, 125);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 200, 700 - 300, 200, 700 - 475);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 200, 700 - 475, 300, 700 - 475);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 300, 700 - 475, 300, 700 - 125);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 300, 700 - 125, 600, 700 - 125);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 400, 125, 400, 700 - 225);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 400, 700 - 225, 500, 700 - 225);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 500, 700 - 225, 500, 700 - 400);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 500, 700 - 400, 690, 700 - 400);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 600, 700 - 125, 600, 700 - 300);
    XDrawLine(xInfo.display, xInfo.window, gc[1].getGC(), 600, 700 - 300, 690, 700 - 300);

    /* Draw top and bottom map cutoff */
    XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), 0, 60, 690, 60);
    XDrawLine(xInfo.display, xInfo.window, gc[0].getGC(), 0, 620, 690, 620);

    /* draw wave tracking bar */
    int j = 0;
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 450, 650, 210, 30); //waveButton 
    drawRectangle(xInfo, gc[0].getGC(), 450, 650, 210, 30);
    for (int i = 0; i < 200; i = i + 20) {
        XDrawArc(xInfo.display, xInfo.window, gc[0].getGC(), 460 + i, 660, 10, 10, 0, 360 * 64);
        if(j<latestwave+1){
            XFillArc(xInfo.display, xInfo.window, gc[0].getGC(), 460 + ((j) * 20), 660, 10, 10, 0, 360 * 64);
        }
        j++;
    }
    
    
    /* coloring buttons */
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 20, 650, 30, 30); // pauseButton
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 27, 655, 6, 20); // pause symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 37, 655, 6, 20); // pause symbol
    
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 53, 650, 30, 30); // playButton
    XPoint points2[3];
    points2[0].x = 58;    points2[0].y = 675;
    points2[1].x = 58;    points2[1].y = 655;
    points2[2].x = 80;    points2[2].y = 665;
    XFillPolygon(xInfo.display, xInfo.window, gc[0].getGC(), points2, 3, Complex, CoordModeOrigin); //play symbol
    
    
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 86, 650, 30, 30); // fastButton
    points2[0].x = 91;    points2[0].y = 675;
    points2[1].x = 91;    points2[1].y = 655;
    points2[2].x = 100;    points2[2].y = 665;
    XFillPolygon(xInfo.display, xInfo.window, gc[0].getGC(), points2, 3, Complex, CoordModeOrigin); //part of fast forward symbol
    points2[0].x = 100;    points2[0].y = 675;
    points2[1].x = 100;    points2[1].y = 655;
    points2[2].x = 109;    points2[2].y = 665;
    XFillPolygon(xInfo.display, xInfo.window, gc[0].getGC(), points2, 3, Complex, CoordModeOrigin); //part of fast forward symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 109, 655, 3, 20); //part of fast forward symbol
    
    
    
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 725, 75, 50, 50); // lightCannonButton
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 80, 10, 40); // lightCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 110, 30, 10); // lightCannon symbol
    
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 725, 140, 50, 50); // heavyCannonButton
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 145, 10, 40); // heavyCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 160, 30, 10); // heavyCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 755, 145, 10, 40); // heavyCannon symbol
    
    XFillRectangle(xInfo.display, xInfo.window, gc[2].getGC(), 725, 205, 50, 50); // superCannonButton
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 210, 30, 9); // superCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 210, 10, 19); // superCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 225, 30, 9); // heavyCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 755, 225, 10, 19); // heavyCannon symbol
    XFillRectangle(xInfo.display, xInfo.window, gc[0].getGC(), 735, 240, 30, 9); // heavyCannon symbol

    //draw buttons
    pauseButton.drawButton(xInfo, gc);
    playButton.drawButton(xInfo, gc);
    fastButton.drawButton(xInfo, gc);
    waveButton.drawButton(xInfo, gc);
    lightCannonButton.drawButton(xInfo, gc);
    heavyCannonButton.drawButton(xInfo, gc);
    superCannonButton.drawButton(xInfo, gc);
}

//Reference for code: Course notes 

void repaint(XInfo &xInfo, CustomGC gc[]) {
    list<Displayable *>::iterator begin = dList.begin();
    list<Displayable *>::iterator end = dList.end();
   
    while (begin != end) {
        Displayable *d = *begin;
        if(d->get_status()==1){
            begin = dList.erase(begin);   
        }else{
            begin++;
        }
    }
    begin = dList.begin();
    end = dList.end();
    
    // draw into the buffer
    // note that a window and a pixmap are “drawables”
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(),
            0, 0, xInfo.width, xInfo.height);
    
        /* Draw Sky */
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 0, 0, 690, 100);
    
    /* Draw water */
        /* Draw waves and water*/
    XPoint points[4];
    for(int i = 0; i < 23; i++){
        points[0].x = i*30;    points[0].y = 50;
        points[1].x = (i*30)+30;    points[1].y = 50;
        points[2].x = (i*30)+50;    points[2].y = 20;
        points[3].x = (i*30)+25;    points[3].y = 30;
        XFillPolygon(xInfo.display, xInfo.pixmap, gc[8].getGC(), points, 4, Complex, CoordModeOrigin);
    }
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 0, 50, 690, 15);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 0, 65, 100, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 0, 340, 300, 220);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 200, 165, 100, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 300, 515, 390, 45);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 400, 65, 290, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 400, 240, 100, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[8].getGC(), 600, 340, 90, 175);
    
    // draw the map
    /* Tunnel */
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 0, 240, 200, 100);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 100, 65, 100, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 200, 65, 200, 100);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 300, 165, 100, 350);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 300, 415, 300, 100);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 500, 240, 100, 175);
    XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), 500, 240, 190, 100);
    
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 0, 240, 100, 240);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 100, 240, 100, 65);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 0, 700 - 360, 200, 700 - 360);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 100, 65, 400, 65);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 200, 700 - 360, 200, 700 - 535);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 200, 700 - 535, 300, 700 - 535);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 300, 700 - 535, 300, 700 - 185);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 300, 700 - 185, 600, 700 - 185);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 400, 65, 400, 700 - 285);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 400, 700 - 285, 500, 700 - 285);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 500, 700 - 285, 500, 700 - 460);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 500, 700 - 460, 690, 700 - 460);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 600, 700 - 185, 600, 700 - 360);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[1].getGC(), 600, 700 - 360, 690, 700 - 360);

    /* Draw top and bottom map cutoff */
    XDrawLine(xInfo.display, xInfo.pixmap, gc[0].getGC(), 0, 0, 690, 0);
    XDrawLine(xInfo.display, xInfo.pixmap, gc[0].getGC(), 0, 560, 690, 560);
    
    if(lightCannonButton.get_Status()==1 || heavyCannonButton.get_Status()==1 || superCannonButton.get_Status()==1){
        drawGrid(xInfo,gc);
        if(towerX > -1 && towerY > -1){
            XDrawRectangle(xInfo.display, xInfo.pixmap, gc[7].getGC(), towerX, towerY-62, towerWidth, towerHeight);
        }
    }
    
    /* Repaint the tower */
    for(int i = 0; i < towers.size(); i++){
        XDrawRectangle(xInfo.display, xInfo.pixmap, gc[1].getGC(), towers[i].getX(), towers[i].getY()-62, towerWidth, towerHeight);
        if(towers[i].getType()==1){
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), towers[i].getX(), towers[i].getY()-62, towerWidth, towerHeight); // lightCannonButton
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-57, 10, towerHeight-10); // lightCannonButton
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-17, towerWidth-20, 10); // lightCannonButton
        }else if(towers[i].getType()==2){
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), towers[i].getX(), towers[i].getY()-62, towerWidth, towerHeight); // heavyCannonButton
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+13, towers[i].getY()-57, 10, towerHeight-10); // heavyCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+13, towers[i].getY()-37, towerHeight-27, 10); // heavyCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+37, towers[i].getY()-57, 10, towerHeight-10); // heavyCannon symbol
        }else if(towers[i].getType()==3){
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[2].getGC(), towers[i].getX(), towers[i].getY()-62, towerHeight, towerHeight); // superCannonButton
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-57, towerWidth-20, 11); // superCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-57, 13, 21); // superCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-38, towerWidth-20, 11); // heavyCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+37, towers[i].getY()-37, 13, 19); // heavyCannon symbol
            XFillRectangle(xInfo.display, xInfo.pixmap, gc[0].getGC(), towers[i].getX()+10, towers[i].getY()-19, towerWidth-20, 11); // heavyCannon symbol

        }
    }
    
    while (begin != end) {
        Displayable *d = *begin;
        d->paint(xInfo, gc); // the displayables know about the pixmap
        begin++;
    }
    // copy buffer to window
    XCopyArea(xInfo.display, xInfo.pixmap, xInfo.window, gc[6].getGC(),
            0, 0, 690, 560, // region of pixmap to copy
            0, 60); // position to put top left corner of pixmap in window

    XFlush(xInfo.display);
}



void putTower(XInfo &xInfo, CustomGC gc[], int x, int y){
    int oldTowerX = towerX;
    int oldTowerY = towerY;
    int isOld = 0;
    
    for(int i = 0; i < towers.size();i++){
        if(towers[0].getX()==oldTowerX && towers[0].getY()==oldTowerY){
            isOld = 1;
        }
    }
    towerX = ((x-(x%20)))-20;
    towerY = ((y-(x%20))+1)-20;
    
    if(isOld == 0 && oldTowerX == towerX && oldTowerY == towerY){
        Tower newTower(towerX, towerY, towerWidth, towerHeight);
        if(lightCannonButton.get_Status()==1){
            newTower.setType(1);
            if(money >= 100){
                money -= 100;
                repaintBalance(xInfo, gc);
            }
        } 
        if(heavyCannonButton.get_Status()==1){
            newTower.setType(2);
            if(money >= 200){
                money -= 200;
                repaintBalance(xInfo, gc);
            }
        } 
        if(superCannonButton.get_Status()==1){
            newTower.setType(3);
            if(money >= 500){
                money -= 500;
                repaintBalance(xInfo, gc);
            }
        }
        newTower.setID(towers.size());
        towers.push_back(newTower);
    }
}

void activateTowers(){
    list<Bullet*>::iterator begin = bullets.begin();
    list<Bullet*>::iterator end = bullets.end();
   
    /* empty bullet list and reload */
    while (begin != end) {
        Bullet *b = *begin;            
        if(b->get_status()==1){

            begin = bullets.erase(begin);
              
        }else{
            begin++;
        }
    }
    int activeTower = 0;
    for(int i = 0; i < towers.size(); i++){
        begin = bullets.begin();
        end = bullets.end();
        while (begin != end) {
            Bullet *b = *begin;            
            if(b->getID()==i){
                activeTower = 1;
            }
            begin++;
        }
        if(activeTower == 0){
            Bullet *bullet = new Bullet(towers[i].getX(), towers[i].getY(), 10);
            bullet->setID(i);
            bullet->setType(towers[i].getType());
            if(towers[i].getType() == 2){
               bullet->setSpeed(24); 
            }else if(towers[i].getType() == 3){
               bullet->setSpeed(30); 
            }
            bullets.push_back(bullet);
            dList.push_front(bullets.back());
        }
        activeTower = 0;        
    }
}

void handleAnimation(XInfo &xinfo, CustomGC gc[]) {
    list<Bullet*>::iterator begin = bullets.begin();
    list<Bullet*>::iterator end = bullets.end();
    //int freezeMonitor = 0;
    for(int i =0; i <= latestwave; i++){
        monster[i].move(xinfo, gc);
    }
    while (begin != end) {
        Bullet *b = *begin;
        if(b->get_status()!=1){
            b->move(xinfo, gc);
        }
        begin++;
    }
    activateTowers();
}
void pauseGame(){
    list<Bullet*>::iterator begin = bullets.begin();
    list<Bullet*>::iterator end = bullets.end();
    while (begin != end) {
        Bullet *b = *begin;
        b->setSpeed(0);
        begin++;
    }
    for(int i =0; i < 10; i++){
        monster[i].setSpeed(0);
    }
}

void resumeGame(){
    list<Bullet*>::iterator begin = bullets.begin();
    list<Bullet*>::iterator end = bullets.end();
    while (begin != end) {
        Bullet *b = *begin;
        if(b->getType()==1){
           b->setSpeed(12); 
        }else if(b->getType()==2){
           b->setSpeed(24);
        }else if(b->getType()==3){
           b->setSpeed(30);
        }
        begin++;
    }
    for(int i =0; i < 10; i++){
        monster[i].setSpeed(4);
    }
}

void fastGame(){
    list<Bullet*>::iterator begin = bullets.begin();
    list<Bullet*>::iterator end = bullets.end();
    while (begin != end) {
        Bullet *b = *begin;
        if(b->getType()==1){
           b->setSpeed(20); 
        }else if(b->getType()==2){
           b->setSpeed(30);
        }else if(b->getType()==3){
           b->setSpeed(40);
        }
        begin++;
    }
    for(int i =0; i < 10; i++){
        monster[i].setSpeed(6);
    }
}

int eventListener(XInfo &xInfo, CustomGC gc[]) {

    XSelectInput(xInfo.display, xInfo.window, ExposureMask | ButtonPressMask | ButtonReleaseMask | KeyPressMask);

    XEvent event;
    Window win;
    unsigned long lastRepaint = 0;
    int x = 0;
    int y = 0;
    int lookup;
    KeySym key;
    char buffer[10];
    int bufferSize=10;
    XComposeStatus compose;
    
    // for events
    while (1) {
        XNextEvent(xInfo.display, &event);
        switch (event.type) {
            case Expose:
                if (event.xexpose.count > 0)
                    break;
		if (splashScreen == 0){
                    drawUI(xInfo, gc);
                    repaint(xInfo, gc);
		}else if(splashScreen == 1){
		    drawSplash(xInfo, gc);		
		} 
                
                break;
            case KeyPress:
                lookup = XLookupString(&event.xkey, buffer, bufferSize,&key, &compose);
                if(lookup > 0){
                    if(buffer[0] == 'm' && splashScreen == 0){
                        money=999999;
                        repaintBalance(xInfo,gc);
                    }else if(buffer[0] == 'p' && splashScreen == 0){
                        pauseGame();
                    }else if(buffer[0] == 'r' && splashScreen == 0){
                        resumeGame();
                    }else if(buffer[0] == 'f' && splashScreen == 0){
                        fastGame();
                    }else if(buffer[0] == 13 && splashScreen == 1){
                        splashScreen = 0;
                        return 0;
                    }else if(buffer[0] == 32 && splashScreen == 0){
                        waveButton.releaseAnimation(xInfo, gc);
                        if (latestwave < 9) {
                            monster[latestwave+1].setWave(latestwave+1);
                            dList.push_front(&monster[latestwave+1]);
                            latestwave++;
                            wave_list[latestwave].set_status(1);
                            XFillArc(xInfo.display, xInfo.window, gc[0].getGC(), 460 + ((latestwave) * 20), 660, 10, 10, 0, 360 * 64);
                            activateTowers();
                        } 
                    }
                }
                break;
            case ButtonPress:
                /* store the mouse button coordinates in 'int' variables. */
                /* also store the ID of the window on which the mouse was */
                /* pressed.                                               */
                x = event.xbutton.x;
                y = event.xbutton.y;
                win = event.xbutton.window;

                /* Pause button press animation*/
                if (x <= 50 && x >= 20 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    pauseButton.clickAnimation(xInfo, gc);
                }

                /* Play button press animation*/
                if (x <= 83 && x >= 53 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    playButton.clickAnimation(xInfo, gc);
                }

                /* Fastforward button press animation*/
                if (x <= 116 && x >= 86 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    fastButton.clickAnimation(xInfo, gc);
                }

                /* Play game button press animation */
                if (x <= 440 && x >= 320 && y <= 680 && y >= 650 && splashScreen == 1) {
                    playGameButton.clickAnimation(xInfo, gc);
                }

                /* Next wave button press animation */
                if (x <= 440 && x >= 320 && y <= 680 && y >= 650 && splashScreen == 0) {
                    waveButton.clickAnimation(xInfo, gc);
                }

                /*light cannon button press animation*/
                if (x <= 775 && x >= 725 && y <= 125 && y >= 75) {
                    lightCannonButton.clickAnimation(xInfo, gc);
                }

                /* heavy cannon button press animation */
                if (x <= 775 && x >= 725 && y <= 190 && y >= 140) {
                    heavyCannonButton.clickAnimation(xInfo, gc);
                }

                /* super cannon button press animation */
                if (x <= 775 && x >= 725 && y <= 255 && y >= 205) {
                    superCannonButton.clickAnimation(xInfo, gc);
                }
                
                

                break;
            case ButtonRelease:
                x = event.xbutton.x;
                y = event.xbutton.y;
                win = event.xbutton.window;

                /* Pause button press animation*/
                if (x <= 50 && x >= 20 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    pauseButton.releaseAnimation(xInfo, gc);
                    pauseGame();
                }

                /* Play button press animation*/
                else if (x <= 83 && x >= 53 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    playButton.releaseAnimation(xInfo, gc);
                    resumeGame();
                }

                /* Fastforward button press animation*/
                else if (x <= 116 && x >= 86 && y <= 680 && y >= 660) { //To fix: make sure this only happens for the window that i want.
                    fastButton.releaseAnimation(xInfo, gc);
                    fastGame();
                }

                /* Play game button press animation */
                else if (x <= 440 && x >= 320 && y <= 680 && y >= 650 && splashScreen == 1) {
                    playGameButton.releaseAnimation(xInfo, gc);
                    splashScreen = 0;
                    return 0;
                }

                /* Next wave button press animation */
                else if (x <= 440 && x >= 320 && y <= 680 && y >= 650 && splashScreen == 0) {
                    waveButton.releaseAnimation(xInfo, gc);
                    if (latestwave < 9) {
                        monster[latestwave+1].setWave(latestwave+1);
                        dList.push_front(&monster[latestwave+1]);
                        latestwave++;
                        wave_list[latestwave].set_status(1);
                        XFillArc(xInfo.display, xInfo.window, gc[0].getGC(), 460 + ((latestwave) * 20), 660, 10, 10, 0, 360 * 64);
                        activateTowers();
                    }
                }

                /*light cannon button press animation*/
                else if (x <= 775 && x >= 725 && y <= 125 && y >= 75) {
                    lightCannonButton.releaseAnimation(xInfo, gc);
                    heavyCannonButton.set_Status(0);
                    superCannonButton.set_Status(0);
                    if(isGameDone()==0){
                        lightCannonButton.drawButtonInfoUI(xInfo, gc);
                        repaint(xInfo, gc);
                    }
                }

                /* heavy cannon button press animation */
                else if (x <= 775 && x >= 725 && y <= 190 && y >= 140) {
                    heavyCannonButton.releaseAnimation(xInfo, gc);
                    lightCannonButton.set_Status(0);
                    superCannonButton.set_Status(0);
                    if(isGameDone()==0){
                        heavyCannonButton.drawButtonInfoUI(xInfo, gc);
                        repaint(xInfo, gc);
                    }
                }

                /* super cannon button press animation */
                else if (x <= 775 && x >= 725 && y <= 255 && y >= 205) {
                    superCannonButton.releaseAnimation(xInfo, gc);
                    lightCannonButton.set_Status(0);
                    heavyCannonButton.set_Status(0);
                    if(isGameDone()==0){
                        superCannonButton.drawButtonInfoUI(xInfo, gc);
                        repaint(xInfo, gc);
                    }
                }
                else {
                    if(x>0 && x<690 && y>60 && y<620 && (lightCannonButton.get_Status()==1 ||heavyCannonButton.get_Status()==1 || superCannonButton.get_Status()==1)){
                        if(lightCannonButton.get_Status()==1 && money>=100){
                            putTower(xInfo, gc, x, y);
                            repaint(xInfo,gc);
                        }
                        if(heavyCannonButton.get_Status()==1 && money>=200){
                            putTower(xInfo, gc, x, y);
                            repaint(xInfo,gc);
                        }
                        if(superCannonButton.get_Status()==1 && money>=500){
                            putTower(xInfo, gc, x, y);
                            repaint(xInfo,gc);
                        }
                    }
                }
                
                break;
            default:
                break;
        }
        if (wave_list[latestwave].get_status() == 1) {
            usleep(100000 / FPS);
            handleAnimation(xInfo, gc);
            repaint(xInfo, gc);
        }
    }
    return 0;
}

/*
 *   Start executing here.
 *	 First initialize window.
 *	 Next loop responding to events.
 *	 Exit forcing window manager to clean up - cheesy, but easy.
 */
int main(int argc, char* argv[]) {

    XInfo xInfo;

    initX(argc, argv, xInfo);
    
    GC gc[12];
    /*
     * Create a graphics Context
     */
    int i = 0;
    gc[i] = XCreateGC(xInfo.display, xInfo.window, 0, 0);

    CustomGC cgc0(xInfo, "#000000", "#FFFFFF", FillSolid, 1, LineSolid, CapButt, JoinRound);
    CustomGC cgc1(xInfo, "#000000", "#FFFFFF", FillSolid, 2, LineSolid, CapButt, JoinRound);
    CustomGC cgc2(xInfo, "#FFFFFF", "#FFFFFF", FillSolid, 1, LineSolid, CapButt, JoinRound);
    CustomGC cgc3(xInfo, "#FFFFFF", "#FFFFFF", FillSolid, 2, LineSolid, CapButt, JoinRound);
    CustomGC cgc4(xInfo, "#000000", "#FFFFFF", FillSolid, 1, LineSolid, CapButt, JoinRound);
    CustomGC cgc5(xInfo, "#BEBEBE", "#FFFFFF", FillSolid, 1 ,LineOnOffDash, CapButt, JoinRound);
    CustomGC cgc6(xInfo, "#FFFFFF", "#000000", FillSolid, 1, LineSolid, CapButt, JoinRound); 
    CustomGC cgc7(xInfo, "#00FF00", "#FFFFFF", FillSolid, 2, LineSolid, CapButt, JoinRound);
    CustomGC cgc8(xInfo, "#A5E1F9", "#A5E1F9", FillSolid, 2, LineSolid, CapButt, JoinRound); //Blue background
    CustomGC cgc9(xInfo, "#F9D7A5", "#F9D7A5", FillSolid, 2, LineSolid, CapButt, JoinRound); //light-brown background
    CustomGC cgc10(xInfo, "#000000", "#F9D7A5", FillSolid, 2, LineSolid, CapButt, JoinRound); // light brown for text
    CustomGC cgc11(xInfo, "#F6546A", "#F6546A", FillSolid, 2, LineSolid, CapButt, JoinRound); // light-red
    CustomGC cgc[12] = {cgc0,cgc1,cgc2,cgc3,cgc4,cgc5,cgc6,cgc7,cgc8,cgc9, cgc10, cgc11};
 
    drawSplash(xInfo, cgc);
    /* flush all pending requests to the X server */
    XFlush(xInfo.display);
    eventListener(xInfo, cgc);
    XClearWindow(xInfo.display, xInfo.window);
    drawUI(xInfo, cgc);
    /* flush all pending requests to the X server */
    XFlush(xInfo.display);
    eventListener(xInfo, cgc);

    // wait for user input to quit (a concole event for now)
    cout << "press ENTER to exit";
    cin.get();
    XCloseDisplay(xInfo.display);
}

